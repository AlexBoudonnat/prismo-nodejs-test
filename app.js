'use strict';

const koa = require('koa');
const cors = require('koa2-cors');
const bodyParser = require('koa-bodyparser');
const koaRouter = require('koa-router');

const app = new koa();
const router = new koaRouter();

const queries = require('./db/queries/members');

app.use(cors());
app.use(bodyParser());

router.get( '/welcome', (ctx) => {
    ctx.body = "Bienvenue chez PRISMO";
});

// Route qui recupère tous les membres dans la BDD
router.get( '/members', async (ctx) => {
    try {
        const members = await queries.getAllMembers();
        ctx.body = {
            status: 'success',
            data: members
        };
    } catch (err) {
        console.log(err)
    }
});

// Route qui recupère un membre en fonction de son id dans la BDD
router.get( '/member/:id', async (ctx) => {
    try {
        const member = await queries.getSingleMember(ctx.params.id);
        if (member.length) {
            ctx.body = {
                status: 'success',
                data: member
            };
        } else {
            ctx.status = 404;
            ctx.body = {
                status: 'error',
                message: 'That movie does not exist.'
            };
        }
    } catch (err) {
        console.log(err)
    }
});

// Route qui permet de créer un membre
router.post('/member', async (ctx) => {
    try {
        const member = await queries.addMember(ctx.request.body);
        if (member.length) {
            ctx.status = 201;
            ctx.body = {
                status: 'success',
                data: member
            };
        } else {
            ctx.status = 400;
            ctx.body = {
                status: 'error',
                message: 'Something went wrong.'
            };
        }
    } catch (err) {
        console.log(err)
    }
});

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(8080);
