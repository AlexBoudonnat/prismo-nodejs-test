const knex = require('knex')(require('../../knexfile'));

function getAllMembers() {
    return knex('member')
        .select('*');
}

function getSingleMember(id) {
    return knex('member')
        .select('*')
        .where({ id: parseInt(id) });
}

function addMember(member) {
    return knex('member')
        .insert({
            firstname: member._firstname,
            lastname: member._lastname,
            avatar: member._avatar,
            function: member._function,
            description: member._description
        });
}

module.exports = {
    getAllMembers,
    getSingleMember,
    addMember,
};
